﻿using ClientApplication.Models;
using System.Collections.Generic;

namespace ClientApplication.SelectionModels
{
    public class UserStruct
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int LastProjectTasksCount { get; set; }
        public List<Task> NotFinishedCanceledTasks { get; set; }
        public Task MaxDurationTask { get; set; }
    }
}
