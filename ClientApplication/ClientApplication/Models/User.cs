﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientApplication.Models
{
    public class User
    {
        public int Id { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime Registered_At { get; set; }
        public List<Task> Tasks { get; set; }
        public List<Project> Projects { get; set; }
        public int Team_Id { get; set; }

        public User()
        {
            Tasks = new List<Task>();
            Projects = new List<Project>();
        }
    }
}
