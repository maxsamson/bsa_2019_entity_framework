﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientApplication.Models
{
    public class Project
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Created_At { get; set; }
        public DateTime Deadline { get; set; }
        public List<Task> Tasks { get; set; }
        public int Author_Id { get; set; }
        public int Team_Id { get; set; }

        public Project()
        {
            Tasks = new List<Task>();
        }

        public override string ToString()
        {
            return $"Id:{this.Id} | Name: {Name}, Description: {Description}";
        }
    }
}
