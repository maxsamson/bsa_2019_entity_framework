﻿using System;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Project_Structure.BLL.Interfaces;
using Project_Structure.BLL.Services;
using Project_Structure.Shared.DTO;
using Project_Structure.Shared.Exceptions;

namespace Project_Structure.Controllers
{
    [Route("api/Selections")]
    [ApiController]
    public class SelectionController : ControllerBase
    {
        private readonly IConfiguration configuration;

        readonly ISelectionService service;
        readonly QueueService queueService;

        public SelectionController(ISelectionService selectionService, QueueService queueService, IConfiguration configuration)
        {
            service = selectionService;
            this.queueService = queueService;
            this.configuration = configuration;
        }

        // GET: api/Selections/CreateHierarhy
        [Route("CreateHierarhy")]
        [HttpGet]
        public IActionResult CreateHierarhy()
        {
            try
            {
                queueService.PostValueWorker("Hierarhy creation was triggered");

                return Ok(service.CreateHierarhy());
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Selections/TasksCount
        [Route("TasksCount/{id:int}")]
        [HttpGet]
        public IActionResult TasksCount(int id)
        {
            try
            {
                queueService.PostValueWorker($"TasksCount with user_Id={id} was triggered");

                return Ok(service.TasksCount(id));
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Selections/TasksWithNameLess45Symbols
        [Route("TasksWithNameLess45Symbols/{id:int}")]
        [HttpGet]
        public IActionResult TasksWithNameLess45Symbols(int id)
        {
            try
            {
                queueService.PostValueWorker($"TasksWithNameLess45Symbols with user_Id={id} was triggered");

                return Ok(service.TasksWithNameLess45Symbols(id));
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Selections/TasksFinished2019
        [Route("TasksFinished2019/{id:int}")]
        [HttpGet]
        public IActionResult TasksFinished2019(int id)
        {
            try
            {
                queueService.PostValueWorker($"TasksFinished2019 with user_Id={id} was triggered");

                return Ok(service.TasksFinished2019(id));
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Selections/Teams12yearsGroup
        [Route("Teams12yearsGroup")]
        [HttpGet]
        public IActionResult Teams12yearsGroup()
        {
            try
            {
                queueService.PostValueWorker($"Teams12yearsGroup was triggered");

                return Ok(service.Teams12yearsGroup());
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Selections/UsersACSTasksDSC
        [Route("UsersACSTasksDSC")]
        [HttpGet]
        public IActionResult UsersACSTasksDSC()
        {
            try
            {
                queueService.PostValueWorker($"UsersACSTasksDSC was triggered");

                return Ok(service.UsersACSTasksDSC());
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Selections/UserStruct
        [Route("UserStruct/{id:int}")]
        [HttpGet]
        public IActionResult UserStruct(int id)
        {
            try
            {
                queueService.PostValueWorker($"UserStruct with user_Id={id} was triggered");

                return Ok(service.UserStruct(id));
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Selections/ProjectStruct
        [Route("ProjectStruct/{id:int}")]
        [HttpGet]
        public IActionResult ProjectStruct(int id)
        {
            try
            {
                queueService.PostValueWorker($"ProjectStruct with project_Id={id} was triggered");

                return Ok(service.ProjectStruct(id));
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Selections/GetMessages
        [Route("GetMessages")]
        [HttpGet]
        public IActionResult GetMessages()
        {
            try
            {
                queueService.PostValueWorker($"GetMessages was triggered");

                return Ok(service.GetMessages(configuration["LogFile"]));
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
    }
}