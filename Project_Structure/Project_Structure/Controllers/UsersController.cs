﻿using System;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Project_Structure.BLL.Interfaces;
using Project_Structure.BLL.Services;
using Project_Structure.Shared.DTO;
using Project_Structure.Shared.Exceptions;

namespace Project_Structure.Controllers
{
    [Route("api/Users")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        readonly IUserService service;
        readonly QueueService queueService;

        public UsersController(IUserService userService, QueueService queueService)
        {
            service = userService;
            this.queueService = queueService;
        }

        // GET: api/Users
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(service.GetAll());
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                queueService.PostValueWorker($"Loading user with id={id} was triggered");

                return Ok(service.GetById(id));
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // POST: v1/api/Users
        [HttpPost]
        public IActionResult Post([FromBody]UserDTO user)
        {
            try
            {
                queueService.PostValueWorker($"User creation was triggered");

                service.Create(user);
                return Ok();
            }
            catch (ValidationException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // PUT: v1/api/Users/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] UserDTO user)
        {
            try
            {
                queueService.PostValueWorker($"User modification with id={id} was triggered");

                service.Update(id, user);
                return Ok();
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
            catch (ValidationException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // DELETE: v1/api/Users/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                queueService.PostValueWorker($"Deleting user with id={id} was triggered");

                service.DeleteById(id);
                return NoContent();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // DELETE: v1/api/Users
        [HttpDelete]
        public IActionResult Delete([FromBody] UserDTO user)
        {
            try
            {
                queueService.PostValueWorker($"Deleting user was triggered");

                service.Delete(user);
                return NoContent();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
    }
}