﻿using System;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Project_Structure.BLL.Interfaces;
using Project_Structure.BLL.Services;
using Project_Structure.Shared.DTO;
using Project_Structure.Shared.Exceptions;

namespace Project_Structure.Controllers
{
    [Route("api/Projects")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        readonly IProjectService service;
        readonly QueueService queueService;

        public ProjectsController(IProjectService projectService, QueueService queueService)
        {
            service = projectService;
            this.queueService = queueService;
        }

        // GET: api/Projects
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(service.GetAll());
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Projects/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                queueService.PostValueWorker($"Loading project with id={id} was triggered");

                return Ok(service.GetById(id));
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // POST: v1/api/Projects
        [HttpPost]
        public IActionResult Post([FromBody]ProjectDTO project)
        {
            try
            {
                queueService.PostValueWorker($"Project creation was triggered");

                service.Create(project);
                return Ok();
            }
            catch (ValidationException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // PUT: v1/api/Projects/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] ProjectDTO project)
        {
            try
            {
                queueService.PostValueWorker($"Project modification with id={id} was triggered");

                service.Update(id, project);
                return Ok();
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
            catch (ValidationException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // DELETE: v1/api/Projects/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                queueService.PostValueWorker($"Deleting project with id={id} was triggered");

                service.DeleteById(id);
                return NoContent();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // DELETE: v1/api/Projects
        [HttpDelete]
        public IActionResult Delete([FromBody] ProjectDTO project)
        {
            try
            {
                queueService.PostValueWorker($"Deleting project was triggered");

                service.Delete(project);
                return NoContent();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
    }
}