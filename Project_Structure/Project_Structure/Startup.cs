﻿using DAL.DBInfrastructure;
using DAL.Repositories;
using FluentValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Project_Structure.BLL.Hubs;
using Project_Structure.BLL.Interfaces;
using Project_Structure.BLL.Mapping;
using Project_Structure.BLL.Services;
using Project_Structure.BLL.Validators;
using Project_Structure.Shared.DTO;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;

namespace Project_Structure
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddScoped<IProjectService, ProjectService>();
            services.AddScoped<ITaskService, TaskService>();
            services.AddScoped<ITeamService, TeamService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ISelectionService, SelectionService>();

            services.AddSingleton<QueueService>();

            services.AddTransient<AbstractValidator<ProjectDTO>, ProjectValidator>();
            services.AddTransient<AbstractValidator<TaskDTO>, TaskValidator>();
            services.AddTransient<AbstractValidator<TeamDTO>, TeamValidator>();
            services.AddTransient<AbstractValidator<UserDTO>, UserValidator>();

            services.AddScoped<ProjectRepository>();
            services.AddScoped<TaskRepository>();
            services.AddScoped<TeamRepository>();
            services.AddScoped<UserRepository>();

            services.AddSingleton<ProjectHub>();

            services.AddTransient<IMapper, Mapper>();

            services.AddSignalR();

            services.AddDbContext<ProjectDbContext>(options => 
                options.UseSqlServer(Configuration.GetConnectionString("ProjectDatabase")));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseSignalR(routes =>
            {
                routes.MapHub<ProjectHub>("/hub");
            });

            app.UseHttpsRedirection();
            app.UseMvc();


        }

        private void ConfigureQueue()
        {
            var factory = new ConnectionFactory
            {
                Uri = new Uri(Configuration["Rabbit"])
            };

            var connection = factory.CreateConnection();
            var channel = connection.CreateModel();
            channel.ExchangeDeclare("WorkerExchange", ExchangeType.Direct);
            channel.QueueDeclare(
                queue: "WorkerQueue",
                durable: true,
                exclusive: false,
                autoDelete: false);
            channel.QueueBind("WorkerQueue", "WorkerExchange", "worker");

            var consumer = new EventingBasicConsumer(channel);
            //consumer.Received += ;

            channel.BasicConsume(
                queue: "WorkerQueue",
                autoAck: false,
                consumer: consumer);
        }
    }
}
