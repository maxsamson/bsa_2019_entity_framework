﻿using DAL.Models;
using DAL.Repositories;
using Project_Structure.BLL.Interfaces;
using Project_Structure.Shared.DTO;
using System.Collections.Generic;

namespace Project_Structure.BLL.Mapping
{
    public class Mapper : IMapper
    {
        private readonly ProjectRepository projectRepo;
        private readonly TaskRepository taskRepo;
        private readonly TeamRepository teamRepo;
        private readonly UserRepository userRepo;

        public Mapper(ProjectRepository projectRepo, TaskRepository taskRepo, TeamRepository teamRepo, UserRepository userRepo)
        {
            this.projectRepo = projectRepo;
            this.taskRepo = taskRepo;
            this.teamRepo = teamRepo;
            this.userRepo = userRepo;
        }

        public ProjectDTO MapProject(Project value)
        {
            var listTasks = new List<TaskDTO>();
            if (value.Tasks != null)
            {
                foreach (var item in value.Tasks)
                    listTasks.Add(MapTask(item));
            }

            return new ProjectDTO()
            {
                Id = value.Id,
                Name = value.Name,
                Description = value.Description,
                Created_At = value.Created_At,
                Deadline = value.Deadline,
                Tasks = listTasks,
                Author_Id = value.Author.Id,
                Team_Id = value.Team.Id
            };
        }

        public Project MapProject(ProjectDTO value)
        {
            var listTasks = taskRepo.GetAll();
            var listUserTasks = new List<Task>();
            if (value.Tasks != null)
            {
                foreach (var item in value.Tasks)
                {
                    foreach (var task in listTasks)
                    {
                        if (item.Id == task.Id)
                        {
                            listUserTasks.Add(task);
                            goto Next1;
                        }
                    }
                Next1:;
                }
            }

            return new Project()
            {
                Id = value.Id,
                Name = value.Name,
                Description = value.Description,
                Created_At = value.Created_At,
                Deadline = value.Deadline,
                Tasks = listUserTasks,
                Author = projectRepo.GetById(value.Id).Author,
                Team = projectRepo.GetById(value.Id).Team
            };
        }

        public List<ProjectDTO> MapProjects(List<Project> valueList)
        {
            List<ProjectDTO> resultList = new List<ProjectDTO>();

            foreach (var item in valueList)
                resultList.Add(MapProject(item));

            return resultList;
        }

        public List<Project> MapProjects(List<ProjectDTO> valueList)
        {
            List<Project> resultList = new List<Project>();

            foreach (var item in valueList)
                resultList.Add(MapProject(item));

            return resultList;
        }

        public TaskDTO MapTask(Task value)
        {
            return new TaskDTO
            {
                Id = value.Id,
                Name = value.Name,
                Description = value.Description,
                Created_At = value.Created_At,
                Finished_At = value.Finished_At,
                State = value.State,
                Project_Id = value.Project.Id,
                Performer_Id = value.User.Id
            };
        }

        public Task MapTask(TaskDTO value)
        {
            return new Task
            {
                Id = value.Id,
                Name = value.Name,
                Description = value.Description,
                Created_At = value.Created_At,
                Finished_At = value.Finished_At,
                State = value.State,
                Project = taskRepo.GetById(value.Id).Project,
                User = taskRepo.GetById(value.Id).User
            };
        }

        public List<TaskDTO> MapTasks(List<Task> valueList)
        {
            List<TaskDTO> resultList = new List<TaskDTO>();

            foreach (var item in valueList)
                resultList.Add(MapTask(item));

            return resultList;
        }

        public List<Task> MapTasks(List<TaskDTO> valueList)
        {
            List<Task> resultList = new List<Task>();

            foreach (var item in valueList)
                resultList.Add(MapTask(item));

            return resultList;
        }

        public TeamDTO MapTeam(Team value)
        {
            var listUsers = new List<UserDTO>();
            if (value.Users != null)
            {
                foreach (var item in value.Users)
                    listUsers.Add(MapUser(item));
            }

            var listProjects = new List<ProjectDTO>();
            if (value.Projects != null)
            {
                foreach (var item in value.Projects)
                    listProjects.Add(MapProject(item));
            }

            return new TeamDTO
            {
                Id = value.Id,
                Name = value.Name,
                Created_At = value.Created_At,
                Users = listUsers,
                Projects = listProjects
            };
        }

        public Team MapTeam(TeamDTO value)
        {
            var listUsers = userRepo.GetAll();
            var listTeamUsers = new List<User>();
            if (value.Users != null)
            {
                foreach (var item in value.Users)
                {
                    foreach (var user in listUsers)
                    {
                        if (item.Id == user.Id)
                        {
                            listTeamUsers.Add(user);
                            goto Next1;
                        }
                    }
                Next1:;
                }
            }

            var listProjects = projectRepo.GetAll();
            var listTeamProjects = new List<Project>();
            if (value.Projects != null)
            {
                foreach (var item in value.Projects)
                {
                    foreach (var project in listProjects)
                    {
                        if (item.Id == project.Id)
                        {
                            listTeamProjects.Add(project);
                            goto Next1;
                        }
                    }
                Next1:;
                }
            }

            return new Team
            {
                Id = value.Id,
                Name = value.Name,
                Created_At = value.Created_At,
                Users = listTeamUsers,
                Projects = listTeamProjects
            };
        }

        public List<TeamDTO> MapTeams(List<Team> valueList)
        {
            List<TeamDTO> resultList = new List<TeamDTO>();

            foreach (var item in valueList)
                resultList.Add(MapTeam(item));

            return resultList;
        }

        public List<Team> MapTeams(List<TeamDTO> valueList)
        {
            List<Team> resultList = new List<Team>();

            foreach (var item in valueList)
                resultList.Add(MapTeam(item));

            return resultList;
        }

        public UserDTO MapUser(User value)
        {
            var listTasks = new List<TaskDTO>();
            if (value.Tasks != null)
            {
                foreach (var item in value.Tasks)
                    listTasks.Add(MapTask(item));
            }

            var listProjects = new List<ProjectDTO>();
            if (value.Projects != null)
            {
                foreach (var item in value.Projects)
                    listProjects.Add(MapProject(item));
            }

            return new UserDTO
            {
                Id = value.Id,
                First_Name = value.First_Name,
                Last_Name = value.Last_Name,
                Email = value.Email,
                Birthday = value.Birthday,
                Registered_At = value.Registered_At,
                Tasks = listTasks,
                Projects = listProjects,
                Team_Id = value.Team.Id
            };
        }

        public User MapUser(UserDTO value)
        {
            var listTasks = taskRepo.GetAll();
            var listUserTasks = new List<Task>();
            if (value.Tasks != null)
            {
                foreach (var item in value.Tasks)
                {
                    foreach (var task in listTasks)
                    {
                        if (item.Id == task.Id)
                        {
                            listUserTasks.Add(task);
                            goto Next1;
                        }
                    }
                Next1:;
                }
            }

            var listProjects = projectRepo.GetAll();
            var listUserProjects = new List<Project>();
            if (value.Projects != null)
            {
                foreach (var item in value.Projects)
                {
                    foreach (var project in listProjects)
                    {
                        if (item.Id == project.Id)
                        {
                            listUserProjects.Add(project);
                            goto Next1;
                        }
                    }
                Next1:;
                }
            }

            return new User
            {
                Id = value.Id,
                First_Name = value.First_Name,
                Last_Name = value.Last_Name,
                Email = value.Email,
                Birthday = value.Birthday,
                Registered_At = value.Registered_At,
                Tasks = listUserTasks,
                Projects = listUserProjects,
                Team = userRepo.GetById(value.Id).Team
            };
        }

        public List<UserDTO> MapUsers(List<User> valueList)
        {
            List<UserDTO> resultList = new List<UserDTO>();

            foreach (var item in valueList)
                resultList.Add(MapUser(item));

            return resultList;
        }

        public List<User> MapUsers(List<UserDTO> valueList)
        {
            List<User> resultList = new List<User>();

            foreach (var item in valueList)
                resultList.Add(MapUser(item));

            return resultList;
        }
    }
}
