﻿using FluentValidation;
using Project_Structure.Shared.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Structure.BLL.Validators
{
    public class TaskValidator : AbstractValidator<TaskDTO>
    {
        public TaskValidator()
        {
            RuleFor(p => p.Name.Length).GreaterThanOrEqualTo(1);
            RuleFor(p => p.Description.Length).GreaterThanOrEqualTo(1);
            RuleFor(p => p.Created_At).LessThanOrEqualTo(DateTime.Now);
            RuleFor(p => p.Finished_At).GreaterThanOrEqualTo(p => p.Created_At);
        }
    }
}
