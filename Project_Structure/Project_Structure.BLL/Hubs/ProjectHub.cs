﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Project_Structure.BLL.Hubs
{
    public sealed class ProjectHub : Hub
    {
        public async Task Send(string value)
        {
            await Clients.All.SendAsync("PostResult", value);
        }
    }
}
