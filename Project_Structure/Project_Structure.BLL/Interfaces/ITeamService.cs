﻿using Project_Structure.Shared.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Structure.BLL.Interfaces
{
    public interface ITeamService
    {
        List<TeamDTO> GetAll();

        TeamDTO GetById(int id);

        int Create(TeamDTO team);

        void Update(int id, TeamDTO team);

        void Delete(TeamDTO team);

        void DeleteById(int id);
    }
}
