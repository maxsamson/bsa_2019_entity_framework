﻿using Project_Structure.Shared.DTO;
using Project_Structure.Shared.SelectionModels;
using System.Collections.Generic;

namespace Project_Structure.BLL.Interfaces
{
    public interface ISelectionService
    {
        List<ProjectDTO> CreateHierarhy();

        List<TasksCount> TasksCount(int user_Id);

        List<TaskDTO> TasksWithNameLess45Symbols(int user_Id);

        List<TasksFinished2019> TasksFinished2019(int user_Id);

        List<Teams12yearsGroup> Teams12yearsGroup();

        List<UsersACSTasksDSC> UsersACSTasksDSC();

        List<UserStruct> UserStruct(int user_Id);

        List<ProjectStruct> ProjectStruct(int proj_Id);

        List<Message> GetMessages(string path);
    }
}
