﻿using DAL.Models;
using Project_Structure.Shared.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Structure.BLL.Interfaces
{
    public interface IMapper
    {
        ProjectDTO MapProject(Project value);
        Project MapProject(ProjectDTO value);
        List<ProjectDTO> MapProjects(List<Project> valueList);
        List<Project> MapProjects(List<ProjectDTO> valueList);

        TaskDTO MapTask(Task value);
        Task MapTask(TaskDTO value);
        List<TaskDTO> MapTasks(List<Task> valueList);
        List<Task> MapTasks(List<TaskDTO> valueList);

        TeamDTO MapTeam(Team value);
        Team MapTeam(TeamDTO value);
        List<TeamDTO> MapTeams(List<Team> valueList);
        List<Team> MapTeams(List<TeamDTO> valueList);

        UserDTO MapUser(User value);
        User MapUser(UserDTO value);
        List<UserDTO> MapUsers(List<User> valueList);
        List<User> MapUsers(List<UserDTO> valueList);
    }
}
