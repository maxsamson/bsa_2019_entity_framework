﻿using Project_Structure.Shared.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Structure.Shared.SelectionModels
{
    public class ProjectStruct
    {
        public ProjectDTO Project { get; set; }
        public TaskDTO LongestTask { get; set; }
        public TaskDTO ShortestTask { get; set; }
        public int UsersCount { get; set; }
    }
}
