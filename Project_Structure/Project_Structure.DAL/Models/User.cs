﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAL.Models
{
    public class User : Entity
    {
        [Required]
        [MaxLength(200)]
        public string First_Name { get; set; }
        [Required]
        [MaxLength(200)]
        public string Last_Name { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public DateTime Birthday { get; set; }
        [Required]
        public DateTime Registered_At { get; set; }
        public List<Task> Tasks { get; set; }
        public List<Project> Projects { get; set; }
        public Team Team { get; set; }
    }
}
