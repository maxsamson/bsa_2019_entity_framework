﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DAL.Models
{
    public enum State { Created, Started, Finished, Canceled }

    public class Task : Entity
    {
        [Required]
        [MaxLength(200)]
        public string Name { get; set; }
        [Required]
        [MaxLength(500)]
        public string Description { get; set; }
        [Required]
        public DateTime Created_At { get; set; }
        public DateTime Finished_At { get; set; }
        [Required]
        public State State { get; set; }
        public Project Project { get; set; }
        public User User { get; set; }
    }
}
