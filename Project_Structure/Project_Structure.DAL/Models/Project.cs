﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAL.Models
{
    public class Project : Entity
    {
        [Required]
        [MaxLength(200)]
        public string Name { get; set; }
        [MaxLength(500)]
        public string Description { get; set; }
        [Required]
        public DateTime Created_At { get; set; }
        public DateTime Deadline { get; set; }
        public List<Task> Tasks { get; set; }
        public User Author { get; set; }
        public Team Team { get; set; }
    }
}
