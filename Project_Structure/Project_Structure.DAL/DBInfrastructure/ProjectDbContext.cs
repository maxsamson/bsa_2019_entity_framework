﻿using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.DBInfrastructure
{
    public class ProjectDbContext : DbContext
    {
        public DbSet<Project> Projects { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }

        public ProjectDbContext(DbContextOptions<ProjectDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.HasSequence<int>("Task_seq", schema: "dbo")
            //.StartsAt(1)
            //.IncrementsBy(1);
            //modelBuilder.Entity<Task>()
            //    .Property(t => t.Id)
            //    .HasDefaultValueSql("NEXT VALUE FOR dbo.Task_seq");

            //modelBuilder.HasSequence<int>("Team_seq", schema: "dbo")
            //.StartsAt(1)
            //.IncrementsBy(1);
            //modelBuilder.Entity<Team>()
            //    .Property(t => t.Id)
            //    .HasDefaultValueSql("NEXT VALUE FOR dbo.Team_seq");

            //modelBuilder.HasSequence<int>("User_seq", schema: "dbo")
            //.StartsAt(1)
            //.IncrementsBy(1);
            //modelBuilder.Entity<User>()
            //    .Property(t => t.Id)
            //    .HasDefaultValueSql("NEXT VALUE FOR dbo.User_seq");

            //modelBuilder.HasSequence<int>("Project_seq", schema: "dbo")
            //.StartsAt(1)
            //.IncrementsBy(1);
            //modelBuilder.Entity<Project>()
            //    .Property(t => t.Id)
            //    .HasDefaultValueSql("NEXT VALUE FOR dbo.Project_seq");

            //modelBuilder.Entity<Task>().HasData(Tasks);
            //modelBuilder.Entity<User>().HasData(Users);
            //modelBuilder.Entity<Team>().HasData(Teams);
            //modelBuilder.Entity<Project>().HasData(Projects);

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<TEntity> SetOf<TEntity>() where TEntity : Entity
        {
            if (Tasks is IEnumerable<TEntity>)
            {
                Projects.Load();
                Users.Load();
                return Tasks as DbSet<TEntity>;
            }
            else if (Teams is IEnumerable<TEntity>)
            {
                Users.Load();
                Projects.Load();
                return Teams as DbSet<TEntity>;
            }
            else if (Projects is IEnumerable<TEntity>)
            {
                Tasks.Load();
                Teams.Load();
                Users.Load();
                return Projects as DbSet<TEntity>;
            }
            else
            {
                Tasks.Load();
                Teams.Load();
                Projects.Load();
                return Users as DbSet<TEntity>;
            }
        }
    }
}
