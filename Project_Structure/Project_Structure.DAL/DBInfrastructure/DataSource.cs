﻿using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.DBInfrastructure
{
    public class DataSource
    {
        private readonly ProjectDbContext projectDbContext;

        private static DataSource dataSource;

        private DataSource(ProjectDbContext context)
        {
            projectDbContext = context;

            projectDbContext.Database.Migrate();

            TasksInit();
            UsersInit();
            TeamsInit();
            ProjectsInit();
        }

        public static DataSource Initialize(ProjectDbContext context)
        {
            if (dataSource == null)
                dataSource = new DataSource(context);
            return dataSource;
        }

        private void TasksInit()
        {
            if (!projectDbContext.Tasks.Any())
            {
                projectDbContext.Tasks.Add(
                    new Task
                    {
                        //Id = 1,
                        Name = "Eaque corporis illum ut.",
                        Description = "Inventore quae alias magnam sed qui sint velit est.\nSuscipit dolor ut omnis voluptate.\nExercitationem qui et temporibus dignissimos autem.\nSapiente recusandae illo maiores quasi.\nEt commodi harum voluptatem cum.",
                        Created_At = new DateTime(2019, 6, 16, 2, 54, 26),
                        Finished_At = new DateTime(2020, 2, 1, 17, 12, 48),
                        State = State.Started
                    });
                projectDbContext.Tasks.Add(
                    new Task
                    {
                        //Id = 2,
                        Name = "Debitis quis ad quas voluptatem voluptatem.",
                        Description = "Similique molestiae esse eius nihil repudiandae possimus eos ea nobis.\nEaque ipsam atque sequi dignissimos fugiat voluptate hic nihil.\nNatus officia quis.\nDeleniti sit earum eveniet.\nCumque magni quis eaque atque natus expedita ratione illum.",
                        Created_At = new DateTime(2019, 6, 18, 19, 31, 15),
                        Finished_At = new DateTime(2019, 8, 9, 3, 39, 45),
                        State = State.Created
                    });
                projectDbContext.Tasks.Add(
                    new Task
                    {
                        //Id = 3,
                        Name = "Officia quas dolorem quod veniam vitae odit voluptates.",
                        Description = "Ut et consequatur quo.\nIpsa rerum qui.\nDelectus sunt voluptatibus facilis facere minima repellat deserunt temporibus.",
                        Created_At = new DateTime(2019, 6, 19, 0, 56, 43),
                        Finished_At = new DateTime(2020, 3, 31, 17, 26, 25),
                        State = State.Started
                    });
                projectDbContext.Tasks.Add(
                    new Task
                    {
                        //Id = 4,
                        Name = "Aut occaecati officiis dolor culpa porro.",
                        Description = "Voluptatem ipsum deleniti quisquam est aut accusamus porro veritatis quia.\nOccaecati et excepturi eius temporibus quia distinctio aspernatur.\nSoluta qui consequuntur est possimus itaque dolores ut.\nFacilis occaecati cupiditate omnis cumque.",
                        Created_At = new DateTime(2019, 6, 19, 1, 1, 47),
                        Finished_At = new DateTime(2019, 7, 7, 16, 54, 47),
                        State = State.Started
                    });
                projectDbContext.Tasks.Add(
                    new Task
                    {
                        //Id = 5,
                        Name = "Eos adipisci dignissimos minus non est est minus.",
                        Description = "Possimus est asperiores repellendus tenetur placeat.\nDolor ipsa facere delectus tempore.\nUt eum esse et dolor delectus repudiandae vitae aut enim.\nDeleniti sint delectus saepe autem.",
                        Created_At = new DateTime(2019, 6, 18, 21, 44, 18),
                        Finished_At = new DateTime(2020, 1, 11, 8, 49, 6),
                        State = State.Finished
                    });

                projectDbContext.SaveChanges();
            }
        }

        private void UsersInit()
        {
            if (!projectDbContext.Users.Any())
            {
                projectDbContext.Users.Add(
                    new User
                    {
                        //Id = 1,
                        First_Name = "Gayle1",
                        Last_Name = "Swift",
                        Email = "Gayle_Swift98@yahoo.com",
                        Birthday = new DateTime(2013, 11, 7, 13, 1, 47),
                        Registered_At = new DateTime(2019, 6, 17, 13, 1, 47),
                        Projects = new List<Project>(),
                        Tasks = new List<Task>(),
                    });
                projectDbContext.SaveChanges();
                projectDbContext.Tasks.Single(t => t.Id == 1).User = projectDbContext.Users.Local.Single(u => u.Id == 1);
                projectDbContext.Tasks.Single(t => t.Id == 2).User = projectDbContext.Users.Single(u => u.Id == 1);
                projectDbContext.Users.Single(u => u.Id == 1).Tasks = new List<Task>(
                    projectDbContext.Tasks.Where(t => t.User.Id == projectDbContext.Users.Single(u => u.Id == 1).Id));

                projectDbContext.Users.Add(
                    new User
                    {
                        //Id = 2,
                        First_Name = "Gayle2",
                        Last_Name = "Swift",
                        Email = "Gayle_Swift98@yahoo.com",
                        Birthday = new DateTime(2013, 11, 7, 13, 1, 47),
                        Registered_At = new DateTime(2019, 6, 17, 13, 1, 47),
                        Projects = new List<Project>(),
                        Tasks = new List<Task>(),
                    });
                projectDbContext.SaveChanges();
                projectDbContext.Tasks.Single(t => t.Id == 3).User = projectDbContext.Users.Single(u => u.Id == 2);
                projectDbContext.Users.Single(u => u.Id == 2).Tasks = new List<Task>(
                    projectDbContext.Tasks.Where(t => t.User.Id == projectDbContext.Users.Single(u => u.Id == 2).Id));

                projectDbContext.Users.Add(
                    new User
                    {
                        //Id = 3,
                        First_Name = "Gayle3",
                        Last_Name = "Swift",
                        Email = "Gayle_Swift98@yahoo.com",
                        Birthday = new DateTime(2013, 11, 7, 13, 1, 47),
                        Registered_At = new DateTime(2019, 6, 17, 13, 1, 47),
                        Projects = new List<Project>(),
                        Tasks = new List<Task>(),
                    });
                projectDbContext.SaveChanges();
                projectDbContext.Tasks.Single(t => t.Id == 4).User = projectDbContext.Users.Single(u => u.Id == 3);
                projectDbContext.Tasks.Single(t => t.Id == 5).User = projectDbContext.Users.Single(u => u.Id == 3);
                projectDbContext.Users.Single(u => u.Id == 3).Tasks = new List<Task>(
                    projectDbContext.Tasks.Where(t => t.User.Id == projectDbContext.Users.Single(u => u.Id == 3).Id));

                projectDbContext.SaveChanges();
            }
        }

        private void TeamsInit()
        {
            if (!projectDbContext.Teams.Any())
            {
                projectDbContext.Teams.Add(
                new Team
                {
                    //Id = 1,
                    Name = "labore",
                    Created_At = new DateTime(2019, 6, 19, 1, 0, 24),
                    Users = new List<User>(),
                    Projects = new List<Project>()
                });
                projectDbContext.SaveChanges();
                projectDbContext.Users.Single(u => u.Id == 1).Team = projectDbContext.Teams.Single(t => t.Id == 1);
                projectDbContext.Users.Single(t => t.Id == 2).Team = projectDbContext.Teams.Single(t => t.Id == 1);
                projectDbContext.Teams.Single(t => t.Id == 1).Users = new List<User>(
                    projectDbContext.Users.Where(u => u.Team.Id == projectDbContext.Teams.Single(t => t.Id == 1).Id));

                projectDbContext.Teams.Add(
                    new Team
                    {
                        //Id = 2,
                        Name = "sunt",
                        Created_At = new DateTime(2019, 6, 19, 6, 38, 3),
                        Users = new List<User>(),
                        Projects = new List<Project>()
                    });
                projectDbContext.SaveChanges();
                projectDbContext.Users.Single(u => u.Id == 3).Team = projectDbContext.Teams.Single(u => u.Id == 2);
                projectDbContext.Teams.Single(t => t.Id == 2).Users = new List<User>(
                    projectDbContext.Users.Where(u => u.Team.Id == projectDbContext.Teams.Single(t => t.Id == 2).Id));

                projectDbContext.SaveChanges();
            }
        }

        private void ProjectsInit()
        {
            if (!projectDbContext.Projects.Any())
            {
                projectDbContext.Projects.Add(
                new Project
                {
                    //Id = 1,
                    Name = "Rerum voluptatem beatae nesciunt consectetur.",
                    Description = "Ut quisquam molestias ut.\nQuia laudantium est quo asperiores veritatis porro ut quod doloribus.\nReiciendis odit ut hic libero.\nFuga debitis veniam ut.\nEos modi dolorem quia accusamus asperiores a et molestiae.\nAut eius et nihil voluptatem.",
                    Created_At = new DateTime(2019, 6, 18, 13, 46, 19),
                    Deadline = new DateTime(2020, 1, 13, 6, 24, 33),
                    Tasks = new List<Task>()
                });
                projectDbContext.SaveChanges();
                projectDbContext.Tasks.Single(t => t.Id == 1).Project = projectDbContext.Projects.Single(u => u.Id == 1);
                projectDbContext.Tasks.Single(t => t.Id == 5).Project = projectDbContext.Projects.Single(u => u.Id == 1);
                projectDbContext.Users.Single(u => u.Id == 1).Projects.Add(projectDbContext.Projects.Single(u => u.Id == 1));
                projectDbContext.Teams.Single(t => t.Id == 2).Projects.Add(projectDbContext.Projects.Single(u => u.Id == 1));
                projectDbContext.Projects.Single(u => u.Id == 1).Author = projectDbContext.Users.Single(u => u.Id == 1);
                projectDbContext.Projects.Single(u => u.Id == 1).Team = projectDbContext.Teams.Single(t => t.Id == 2);
                projectDbContext.Projects.Single(p => p.Id == 1).Tasks = new List<Task>(
                    projectDbContext.Tasks.Where(t => t.Project.Id == projectDbContext.Projects.Single(p => p.Id == 1).Id));

                projectDbContext.Projects.Add(
                    new Project
                    {
                        //Id = 2,
                        Name = "Atque quia et optio aut.",
                        Description = "Non non aperiam aspernatur et est mollitia enim quia nihil.\nLibero adipisci quisquam earum fugiat et perferendis explicabo.",
                        Created_At = new DateTime(2019, 6, 18, 20, 12, 38),
                        Deadline = new DateTime(2020, 5, 9, 12, 34, 30),
                        Tasks = new List<Task>()
                    });
                projectDbContext.SaveChanges();
                projectDbContext.Tasks.Single(t => t.Id == 2).Project = projectDbContext.Projects.Single(u => u.Id == 2);
                projectDbContext.Tasks.Single(t => t.Id == 4).Project = projectDbContext.Projects.Single(u => u.Id == 2);
                projectDbContext.Users.Single(u => u.Id == 2).Projects.Add(projectDbContext.Projects.Single(u => u.Id == 2));
                projectDbContext.Teams.Single(t => t.Id == 1).Projects.Add(projectDbContext.Projects.Single(u => u.Id == 2));
                projectDbContext.Projects.Single(u => u.Id == 2).Author = projectDbContext.Users.Single(u => u.Id == 2);
                projectDbContext.Projects.Single(u => u.Id == 2).Team = projectDbContext.Teams.Single(t => t.Id == 1);
                projectDbContext.Projects.Single(p => p.Id == 2).Tasks = new List<Task>(
                    projectDbContext.Tasks.Where(t => t.Project.Id == projectDbContext.Projects.Single(p => p.Id == 2).Id));

                projectDbContext.Projects.Add(
                    new Project
                    {
                        //Id = 3,
                        Name = "Harum rerum ipsa quidem et.",
                        Description = "Cupiditate veritatis ad rerum veritatis molestiae.\nDolorem soluta adipisci fuga iste aut est eveniet et.\nMolestiae ut nisi officia nobis consequatur neque sint.\nAut in non officia illo minus.\nVoluptas ut est qui dolore distinctio sint delectus.",
                        Created_At = new DateTime(2019, 6, 18, 23, 17, 38),
                        Deadline = new DateTime(2019, 9, 14, 16, 45, 32),
                        Tasks = new List<Task>()
                    });
                projectDbContext.SaveChanges();
                projectDbContext.Tasks.Single(t => t.Id == 3).Project = projectDbContext.Projects.Single(u => u.Id == 3);
                projectDbContext.Users.Single(u => u.Id == 2).Projects.Add(projectDbContext.Projects.Single(u => u.Id == 3));
                projectDbContext.Teams.Single(t => t.Id == 2).Projects.Add(projectDbContext.Projects.Single(u => u.Id == 3));
                projectDbContext.Projects.Single(u => u.Id == 3).Author = projectDbContext.Users.Single(u => u.Id == 2);
                projectDbContext.Projects.Single(u => u.Id == 3).Team = projectDbContext.Teams.Single(t => t.Id == 2);
                projectDbContext.Projects.Single(p => p.Id == 3).Tasks = new List<Task>(
                    projectDbContext.Tasks.Where(t => t.Project.Id == projectDbContext.Projects.Single(p => p.Id == 3).Id));

                projectDbContext.SaveChanges();
            }
        }
    }
}
