﻿using DAL.DBInfrastructure;
using DAL.Interfaces;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace DAL.Repositories
{
    public class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        //protected readonly PastDBContext projectDbContext;
        protected readonly ProjectDbContext projectDbContext;

        public BaseRepository(ProjectDbContext dbContext)
        {
            projectDbContext = dbContext;

            DataSource.Initialize(projectDbContext);
        }

        public virtual int Create(TEntity entity)
        {
            entity.Id = projectDbContext.SetOf<TEntity>().Count() + 1;
            projectDbContext.SetOf<TEntity>().Add(entity);
            return entity.Id;
        }

        public virtual void Delete(TEntity entity)
        {
            projectDbContext.SetOf<TEntity>().Remove(entity);
        }

        public virtual void DeleteById(int id)
        {
            projectDbContext.SetOf<TEntity>().Remove(projectDbContext.SetOf<TEntity>().Single(entity => entity.Id == id));
        }

        public virtual List<TEntity> GetAll()
        {
            return projectDbContext.SetOf<TEntity>().ToList();
        }

        public virtual TEntity GetById(int id)
        {
            return projectDbContext.SetOf<TEntity>().Single(entity => entity.Id == id);
        }

        public virtual void Update(int id, TEntity entity)
        {
            var temp = projectDbContext.SetOf<TEntity>().Single(x => x.Id == id);
            temp = entity;
        }
    }
}
