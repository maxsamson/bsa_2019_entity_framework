﻿using DAL.DBInfrastructure;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories
{
    public class TaskRepository : BaseRepository<Task>
    {
        public TaskRepository(ProjectDbContext dBContext) : base(dBContext)
        {
            
        }
    }
}
